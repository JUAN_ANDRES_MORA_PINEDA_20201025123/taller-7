let turno=1;
let fichas=["O", "X"];
let puestas=0;
let partidadacabada=false;
let texto_victoria=document.getElementById("texto_victoria");
let botones=Array.from(document.getElementsByTagName("button"));//recoge todos los botones de la pagina web
botones.forEach(x=>x.addEventListener("click", ponersimbolo));//cuando se de click se ejecutara la funcion ponersimbolo

function ponersimbolo(event){
    let botonpulsado = event.target;
    //comprobacion de la partida y que el boton no sea pulsado
    if(!partidadacabada && botonpulsado.innerHTML==""){
        botonpulsado.innerHTML = fichas[turno];
        puestas+=1;
        let estadopartida=estado();
        if(estadopartida==0){
            cambiarturno();
            if(puestas<9){
                computadora();
                estadopartida=estado();
                puestas +=1;
                cambiarturno();
            }
        }
        if(estadopartida==1){
            texto_victoria.style.visibility="visible";
            partidadacabada= true;
        }
        else if(estadopartida==-1){
            texto_victoria.innerHTML="Has perdido";
            partidadacabada=true;
            texto_victoria.style.visibility="visible";
        }
    }
}

function estado(){
    posicionvictoria=0;
    nestado=0;
    //Comprobacion de que los elementos son iguales para determinar el ganador y que no pueden estar en blanco
    function soniguales(...args){
        valores=args.map(x=>x.innerHTML);
        if(valores[0] !="" && valores.every((x, i, arr)=> x===arr[0])){
            args.forEach(x=>x.style.backgroundColor="salmon");
            return true;
        }
        else{
            return false;
        }
    }
    //Comprobar si hay ganador
    if(soniguales(botones[0], botones[1], botones[2])){
        posicionvictoria=1;
    }else if(soniguales(botones[3], botones[4], botones[5])){
        posicionvictoria=2;
    }else if(soniguales(botones[6], botones[7], botones[8])){
        posicionvictoria=3;
    }else if(soniguales(botones[0], botones[3], botones[6])){
        posicionvictoria=4;
    }else if(soniguales(botones[1], botones[4], botones[7])){
        posicionvictoria=5;
    }else if(soniguales(botones[2], botones[5], botones[8])){
        posicionvictoria=6;
    }else if(soniguales(botones[0], botones[4], botones[8])){
        posicionvictoria=7;
    }else if(soniguales(botones[2], botones[4], botones[6])){
        posicionvictoria=8;
    }
    //comprobar quien gano
    if(posicionvictoria>0){
        if(turno==1){
            nestado=1;
        }
        else{
            nestado=-1;
        }
    }
    return nestado;
}
//Funcion para definir turno
function cambiarturno(){
    if(turno==1){
        turno=0;
    }
    else{
        turno=1;
    }
}
//Funcion de la computadora para generar una opcion al azar
function computadora(){
    function aleatorio(min, max){
        return Math.floor(Math.random()*(max-min+1))+min;
    }
    let valores = botones.map(x=>x.innerHTML);
    let pos= -1;
    //Si la posicion del medio esta vacia ocuapara esa primera ya que es la de mayor probabilidad de ganar
    //y si no buscara un lugar libre
    if(valores[4]==""){
        pos=4;
    }
    else{
        let n=aleatorio(0, botones,length-1);
        while(valores[n]!=""){
            n=aleatorio(0, botones.length-1);
        }
        pos =n;
    }
    botones[pos].innerHTML="O";
    return pos;
}




/*function dibujartablero(){
    let i;
    let miTablero=document.getElementById("tablero");
    for(i=1; i<10; i++){
        let miCasilla=document.createElement("input");
        miCasilla.setAttribute("type", "button");
        miCasilla.setAttribute("class", "casilla");
        miCasilla.setAttribute("id", "i");
        miCasilla.setAttribute("onClick", "definirjugador()")
        function definirjugador(){
            if(i%2==0){
                function d_t2(){
                    j1=document.getElementById("i");
                    j1.innerText="Y";
                }
            }else{
                function d_t(){
                    j1=document.getElementById("i");
                    j1.innerText="X";
                }
            }
        }
        miTablero.appendChild(miCasilla);
        if(i%3==0){
            let espacio=document.createElement("br");
            miTablero.appendChild(espacio);
        }
    }
}
/*
let turno =0;
tablero=[];
function d_t(elemento, posicion){
    turno++;
    btn=elemento.target;
    jindef=document.getElementById("tablero");
    jindef.innerText=turno%2? 'X':'Y';
}

document.querySelectorAll('button').forEach((Objeto, i)=>Objeto.addEventListener('click', (elemento)=>
d_t(elemento, i)));
*/
/*
  <div class="container">
    <button id="i" onclick="jugadores()"></button>
    <button id="i" onclick="jugadores()"></button>
    <button id="i" onclick="jugadores()"></button>
    <button id="i" onclick="jugadores()"></button>
    <button id="i" onclick="jugadores()"></button>
    <button id="i" onclick="jugadores()"></button>
    <button id="i" onclick="jugadores()"></button>
    <button id="i" onclick="jugadores()"></button>
    <button id="i" onclick="jugadores()"></button>
    </div>
*/